package com.shop.libs.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.shop.libs.beans.ProductBean;
import com.shop.libs.beans.ReceiptResponseBean;
import com.shop.libs.service.impl.CartServiceImpl;

@RunWith(SpringRunner.class)
public class CartServiceImplTest {

	@InjectMocks
	private CartServiceImpl cartService;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setup(){
		cartService = new CartServiceImpl();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void cartCheckout() {
		List<ProductBean> beanList = new ArrayList<ProductBean>();
		ProductBean productBean = new ProductBean();
		productBean.setCategory("A");
		productBean.setDescription("SONY BRAVIA");
		productBean.setId(123);
		productBean.setName("SONY");
		productBean.setPrize(45999);
		productBean.setQuantity(1);
		beanList.add(productBean);
		ReceiptResponseBean actualResponse = cartService.cartCheckout(beanList);
		assertEquals(actualResponse.getItemsList().get(0).getName(), "SONY");
		assertEquals(actualResponse.getItemsList().get(0).getQuantity(), 1);
		assertEquals(45999,actualResponse.getItemsList().get(0).getPrize(),2);
		assertEquals(46045,actualResponse.getItemsList().get(0).getPrizeWithTax(),2);
		assertEquals(4599,actualResponse.getItemsList().get(0).getSalesTax(),2);
	}
	@Test(expected = RuntimeException.class)
	public void cartCheckoutWithDublicateProduct() {
		List<ProductBean> beanList = new ArrayList<ProductBean>();
		ProductBean productBean = new ProductBean();
		productBean.setId(123);
		beanList.add(productBean);
		ProductBean productBean1 = new ProductBean();
		productBean1.setId(123);
		beanList.add(productBean1);
		cartService.cartCheckout(beanList);
	}
	
	@Test
	public void cartCheckoutWithEmpty() {
		List<ProductBean> beanList = new ArrayList<ProductBean>();
		ReceiptResponseBean actualResponse = cartService.cartCheckout(beanList);
		assertNull(actualResponse);
	}
}
