package com.shop.libs.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.shop.libs.beans.BillItem;
import com.shop.libs.beans.ProductBean;
import com.shop.libs.beans.ProductRequestBean;
import com.shop.libs.beans.ReceiptResponseBean;
import com.shop.libs.controller.impl.CartControllerImpl;
import com.shop.libs.service.CartService;

@RunWith(SpringRunner.class)
public class CartControllerImplTest {

	@Mock
	private CartService service;
	
	@InjectMocks
	private CartControllerImpl cartController;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setup(){
		cartController = new CartControllerImpl();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCartCheckout(){
		
		ProductRequestBean request = new ProductRequestBean();
		List<ProductBean> list = new ArrayList<>();
		request.setBeanList(list);
		
		ReceiptResponseBean response = new ReceiptResponseBean();
		List<BillItem> itemsList = new ArrayList<>();
		BillItem item = new BillItem();
		item.setName("PowerBank");
		item.setPrize(1000);
		item.setPrizeWithTax(2200.22);
		item.setQuantity(2);
		item.setSalesTax(200);
		itemsList.add(item);
		response.setItemsList(itemsList);
		
		Mockito.when(service.cartCheckout(list)).thenReturn(response);
		ReceiptResponseBean actualResponse = cartController.cartCheckout(request);
		
		assertNotNull(actualResponse);
		assertEquals(actualResponse.getItemsList().get(0).getName(), "PowerBank");
		assertEquals(2200, actualResponse.getItemsList().get(0).getPrizeWithTax(),2);
		assertEquals(1000, actualResponse.getItemsList().get(0).getPrize(),0);
		assertEquals(2, actualResponse.getItemsList().get(0).getQuantity());
		assertEquals(200, actualResponse.getItemsList().get(0).getSalesTax(), 2);
	}
	
	@Test
	public void testCartCheckoutWithNull(){
		
		ProductRequestBean request = new ProductRequestBean();
		List<ProductBean> list = new ArrayList<>();
		ReceiptResponseBean response = null;
		
		Mockito.when(service.cartCheckout(list)).thenReturn(response);
		ReceiptResponseBean actualResponse = cartController.cartCheckout(request);
		
		assertNull(actualResponse);
	}
}
