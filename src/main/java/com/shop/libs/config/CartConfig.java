package com.shop.libs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.shop.libs.helper.CartHelper;
import com.shop.libs.service.CartService;
import com.shop.libs.service.impl.CartServiceImpl;

@Configuration
public class CartConfig {

	@Bean
	public CartService getCartService(){
		return new CartServiceImpl();
	}
	
	@Bean
	public CartHelper getCartHelper(){
		return new CartHelper();
	}
	
}
