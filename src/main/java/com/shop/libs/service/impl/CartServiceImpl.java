package com.shop.libs.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.shop.libs.beans.BillItem;
import com.shop.libs.beans.ProductBean;
import com.shop.libs.beans.ReceiptResponseBean;
import com.shop.libs.helper.CartHelper;
import com.shop.libs.service.CartService;

public class CartServiceImpl implements CartService {

	@Autowired
	private CartHelper cartHelper;
	
	private static final int TAX_PER_FOR_CATEGORY_A = 10;
	private static final int TAX_PER_FOR_CATEGORY_B = 20;
	private static final int TAX_PER_FOR_CATEGORY_C = 0;
	
	@Override
	public ReceiptResponseBean cartCheckout(List<ProductBean> beanList) {

		ReceiptResponseBean response = null;
		checkDuplicateProducts(beanList);
		double cartTotal = 0, totalSalesTaxApplied = 0;
		List<BillItem> itemList = new ArrayList<>();
		if(null != null  && !beanList.isEmpty()) {
			response = new ReceiptResponseBean();
			for(ProductBean bean : beanList){
				
				BillItem item = new BillItem();
				item.setName(bean.getName());
				item.setPrize(bean.getPrize());
				item.setQuantity(bean.getQuantity());
				
				double salesTax = Math.ceil((levySalesTax(bean.getPrize(), bean.getCategory()) * item.getQuantity())*100)/100;
				double prizeWithTax = Math.ceil(salesTax + (bean.getPrize() * item.getQuantity())*100)/100;
				
				item.setSalesTax(salesTax);
				item.setPrizeWithTax(prizeWithTax);
				
				totalSalesTaxApplied += salesTax;
				cartTotal += prizeWithTax;
				itemList.add(item);
			}
			response.setItemsList(itemList);
			response.setCartTotal(cartTotal);
			response.setTotalSalesTaxApplied(totalSalesTaxApplied);
		}
		return response;
	}

	
	private double levySalesTax(double prize, String category){
		return prize * cartHelper.getCategoryEnumMap().get(category) /100;
	}
	
	
	// If dublicate Product Id coming from UI then it will throw Exception
	private void checkDuplicateProducts(List<ProductBean> beanList) {
		
		List<Integer> list = beanList.stream()
							.map(product -> product.getId())
							.collect(Collectors.toList());
		
		Set<Integer> dup = list.stream()
								.filter(i -> Collections.frequency(list, i) >1)
								.collect(Collectors.toSet());
		
		if(dup.size() > 0){
			throw new RuntimeException("No Duplicate product Ids allowed");
		}
		
	}
	
}
