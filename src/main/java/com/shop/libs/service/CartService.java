package com.shop.libs.service;

import java.util.List;

import com.shop.libs.beans.ProductBean;
import com.shop.libs.beans.ReceiptResponseBean;

public interface CartService {

	public ReceiptResponseBean cartCheckout(List<ProductBean> beanList);
	
}
