package com.shop.libs.helper;

import java.util.EnumMap;

public class CartHelper {

	public enum CATEGORY 
    { 
        A,B,C; 
    } 
	public EnumMap<CATEGORY,Integer> getCategoryEnumMap(){
		EnumMap<CATEGORY,Integer> catagoryMap = new 
                EnumMap<CATEGORY, Integer>(CATEGORY.class);
		catagoryMap.put(CATEGORY.A, 10);
		catagoryMap.put(CATEGORY.B, 20);
		catagoryMap.put(CATEGORY.C, 0);
		return catagoryMap;
	}
}
