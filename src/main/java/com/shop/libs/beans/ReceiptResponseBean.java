package com.shop.libs.beans;

import java.util.List;

public class ReceiptResponseBean {

	List<BillItem> itemsList;
	private double totalSalesTaxApplied;
	private double cartTotal;
	
	public List<BillItem> getItemsList() {
		return itemsList;
	}
	public void setItemsList(List<BillItem> itemsList) {
		this.itemsList = itemsList;
	}
	public double getTotalSalesTaxApplied() {
		return totalSalesTaxApplied;
	}
	public void setTotalSalesTaxApplied(double totalSalesTaxApplied) {
		this.totalSalesTaxApplied = totalSalesTaxApplied;
	}
	public double getCartTotal() {
		return cartTotal;
	}
	public void setCartTotal(double cartTotal) {
		this.cartTotal = cartTotal;
	}
	
}
