package com.shop.libs.beans;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ProductRequestBean {

	@NotNull(message = "bean list cannot be null")
	@Valid
	List<ProductBean> beanList;

	public List<ProductBean> getBeanList() {
		return beanList;
	}

	public void setBeanList(List<ProductBean> beanList) {
		this.beanList = beanList;
	}
	
}
