package com.shop.libs.beans;

public class BillItem {
	
	private String name;
	private double prize;
	private int quantity;
	private double salesTax;
	private double prizeWithTax;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getSalesTax() {
		return salesTax;
	}
	public void setSalesTax(double salesTax) {
		this.salesTax = salesTax;
	}
	public double getPrizeWithTax() {
		return prizeWithTax;
	}
	public void setPrizeWithTax(double prizeWithTax) {
		this.prizeWithTax = prizeWithTax;
	}

}
