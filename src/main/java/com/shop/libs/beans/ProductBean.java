package com.shop.libs.beans;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ProductBean {
	
	@NotNull(message = "Product ID cannot be null")
	private int id;
	
	@NotNull(message = "Product Name cannot be null")
	private String name;
	
	@NotNull(message = "Product Description cannot be null")
	private String description;
	
	@NotNull(message = "Product Prize cannot be null")
	private double prize;
	
	@NotNull(message = "Name cannot be null")
	@Pattern(regexp="[ABC]", message = "Invalid product category")
	private String category;
	
	@NotNull(message = "Qantity cannot be null")
	private int quantity;
	
	public int getId() {
		return id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
}
