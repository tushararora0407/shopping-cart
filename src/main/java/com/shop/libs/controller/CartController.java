package com.shop.libs.controller;

import com.shop.libs.beans.ProductRequestBean;
import com.shop.libs.beans.ReceiptResponseBean;

public interface CartController {
	
	public ReceiptResponseBean cartCheckout(ProductRequestBean request);
	
}
