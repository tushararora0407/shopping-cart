package com.shop.libs.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.libs.beans.ProductRequestBean;
import com.shop.libs.beans.ReceiptResponseBean;
import com.shop.libs.controller.CartController;
import com.shop.libs.service.CartService;

@RestController
@RequestMapping(value="/cart")
public class CartControllerImpl implements CartController{

	@Autowired
	private CartService cartService;
	
	@Override
	@PostMapping("/checkout")
	public ReceiptResponseBean cartCheckout(@Valid @RequestBody ProductRequestBean request) {
		return cartService.cartCheckout(request.getBeanList());
	}

}
